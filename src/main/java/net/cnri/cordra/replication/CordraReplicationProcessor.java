package net.cnri.cordra.replication;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.cnri.cordra.CordraHooksSupport;
import net.cnri.cordra.CordraHooksSupportProvider;
import net.cnri.cordra.api.*;
import net.cnri.cordra.doip.WrappingTokenUsingCordraClient;
import net.dona.doip.client.ServiceInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class CordraReplicationProcessor {
    private static Logger logger = LoggerFactory.getLogger(CordraReplicationProcessor.class);
    private static CordraReplicationProcessor instance;
    private final CordraClient localCordra;
    private volatile boolean running = false;
    private volatile boolean shutdown = false;
    private boolean verbose = true;
    private final ScheduledThreadPoolExecutor exec;
    private static final Long DEFAULT_INTERVAL_SECONDS = 60L;

    public synchronized static CordraReplicationProcessor getInstance() {
        if (instance == null) {
            instance = new CordraReplicationProcessor();
        }
        return instance;
    }

    private CordraReplicationProcessor() {
        CordraHooksSupport hooks = CordraHooksSupportProvider.get();
        hooks.addShutdownHook(this::shutdown);
        this.localCordra = hooks.getCordraClient();
        this.exec = (ScheduledThreadPoolExecutor) Executors.newScheduledThreadPool(1);
        this.exec.setExecuteExistingDelayedTasksAfterShutdownPolicy(false);
        hooks.addShutdownHook(this::shutdown);
    }

    private CordraReplicationProcessor(CordraClient localCordra) {
        this.localCordra = localCordra;
        this.exec = (ScheduledThreadPoolExecutor) Executors.newScheduledThreadPool(1);
        this.exec.setExecuteExistingDelayedTasksAfterShutdownPolicy(false);
    }

    public synchronized void start() {
        if (running) {
            return;
        }
        running = true;
        scheduleNextRun(0L);
    }

    public synchronized void stop() {
        running = false;
    }

    public synchronized void shutdown() {
        if (shutdown) {
            return;
        }
        shutdown = true;
        running = false;
        exec.shutdown();
        try {
            exec.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
        } catch (InterruptedException e) {
            logger.error("Failure to shutdown CordraReplicationProcessor", e);
        }
    }

    private void scheduleNextRun(Long delay) {
        if (!running) {
            return;
        }
        if (verbose) System.out.println("in scheduleNextRun");
        exec.schedule(() -> {
            long nextDelay = DEFAULT_INTERVAL_SECONDS;
            try {
                CordraObject stateCo = getReplicationStateCordraObject();
                CordraReplicationState state = stateCo.getContent(CordraReplicationState.class);
                if (verbose) System.out.println("got state object");
                if (state.pollingIntervalSeconds != null) {
                    nextDelay = state.pollingIntervalSeconds;
                }
                replicateFromAll(state);
            } catch (CordraException e) {
                logger.error("Replication error ", e);
            }
            scheduleNextRun(nextDelay);
        }, delay, TimeUnit.SECONDS);
    }

    private CordraObject getReplicationStateCordraObject() throws CordraException {
        CordraObject result = null;
        try (SearchResults<CordraObject> results = localCordra.search("type:CordraReplicationState")) {
            Iterator<CordraObject> iter = results.iterator();
            if (iter.hasNext()) {
                result = iter.next();
            }
        }
        return result;
    }

    public void replicateFromAll(CordraReplicationState state) {
        for (Service service : state.services) {
            if (!running) {
                return;
            }
            replicateFrom(service);
        }
    }

    public void replicateFrom(Service service) {
        if (verbose) System.out.println("in replicateFrom " + service.id);
        Long latestSuccessfulTxnId = null;
        String currentId = null;
        Long currentTxnId = null;
        String error = null;
        try (CordraClient remoteCordra = getCordraClientFor(service)) {
            Long highestSafeTxnIdForSearch = getHighestSafeTxnIdForSearch(remoteCordra);
            String query = ReplicationQuery.queryFor(service, highestSafeTxnIdForSearch);
            if (verbose) System.out.println(query);
            SortField sortByTxnId = new SortField("metadata/txnId", false);
            List<SortField> sortFields = new ArrayList<>();
            sortFields.add(sortByTxnId);
            QueryParams params = new QueryParams(0, -1, sortFields);
            try (SearchResults<CordraObject> results = remoteCordra.search(query, params)) {
                if (verbose) System.out.println("There are " + results.size() + " results");
                for (CordraObject co : results) {
                    if (!running) {
                        break;
                    }
                    currentId = co.id;
                    currentTxnId = co.metadata.txnId;
                    attachReplicationSource(co, service);
                    CordraObjectCopyUtil.copyRemoteObjectToLocal(co, remoteCordra, localCordra);
                    latestSuccessfulTxnId = co.metadata.txnId; // only set if successful copy
                    if (verbose) System.out.println("copied " + co.id);
                }
            }
        } catch (IOException | CordraException e) {
            if (currentId != null) {
                error = "Error replicating object: " + currentId + " txnId: " + currentTxnId  + " msg: " + e.getMessage();
                if (verbose) System.out.println(error);
            } else {
                error = "Error replicating from " + service.id  + " msg: " + e.getMessage();
                if (verbose) System.out.println(error);
            }
            logger.error(error, e);
        }
        try {
            recordStateForService(latestSuccessfulTxnId, service.id, error);
        } catch (CordraException e) {
            logger.error("Replication error, unable to record state", e);
        }
    }

    private CordraClient getCordraClientFor(Service service) throws CordraException {
        CordraClient remoteCordra;
        if ("DOIP".equals(service.protocol)) {
            if (verbose) System.out.println("Using DOIP client");
            ServiceInfo serviceInfo = new ServiceInfo();
            serviceInfo.serviceId = service.id;
            serviceInfo.ipAddress = service.ipAddress;
            serviceInfo.port = service.port;
            remoteCordra = WrappingTokenUsingCordraClient.createWithNativeDoipCordraClient(serviceInfo, service.username, service.password);
        } else if ("DOIP-HTTP".equals(service.protocol)) {
            if (verbose) System.out.println("Using DOIP-HTTP client");
            if (service.baseUri != null) {
                remoteCordra = WrappingTokenUsingCordraClient.createWithHttpDoipCordraClient(service.baseUri, service.username, service.password);
            } else {
                ServiceInfo serviceInfo = new ServiceInfo();
                serviceInfo.serviceId = service.id;
                serviceInfo.ipAddress = service.ipAddress;
                serviceInfo.port = service.port;
                remoteCordra = WrappingTokenUsingCordraClient.createWithHttpDoipCordraClient(serviceInfo, service.username, service.password);
            }
        } else {
            if (verbose) System.out.println("Using CORDRA-HTTP client");
            remoteCordra = new TokenUsingHttpCordraClient(service.baseUri, service.username, service.password);
        }
        return remoteCordra;
    }

    private void attachReplicationSource(CordraObject co, Service service) {
        if (co.userMetadata == null) {
            co.userMetadata = new JsonObject();
        }
        JsonObject replicationProvenance = new JsonObject();
        replicationProvenance.addProperty("sourceId", service.id);
        co.userMetadata.add("cordraReplication", replicationProvenance);
    }

    private void recordStateForService(Long latestTxnId, String serviceId, String error) throws CordraException {
        if (verbose) {
            if (latestTxnId != null) {
                System.out.println("recordStateForService " + serviceId + " " + latestTxnId);
            } else {
                System.out.println("recordStateForService " + serviceId + " no txnId change");
            }
        }
        //TODO consider update status object for every id
        //or throttle and store occasionally.
        CordraObject stateCo = getReplicationStateCordraObject();
        CordraReplicationState state = stateCo.getContent(CordraReplicationState.class);
        Service service = state.getService(serviceId);
        if (service == null) {
            throw new InternalErrorCordraException("Missing replication serviceId " + serviceId + " in state object");
        }
        //TODO maybe only update the id if it is different
        if (latestTxnId != null) {
            service.latestTxnId = latestTxnId;
        }
        service.lastRunTimestamp = Instant.now().toString();
        service.error = error;
        stateCo.setContent(state);
        localCordra.update(stateCo);
    }

    private Long getHighestSafeTxnIdForSearch(CordraClient remoteCordra) throws CordraException {
        JsonElement resultJson = remoteCordra.call("design", "20.DOIP/Op.HighestSafeTxnIdForSearch", new JsonObject());
        JsonObject result = resultJson.getAsJsonObject();
        Long highestSafeTxnIdForSearch = result.get("highestSafeTxnIdForSearch").getAsLong();
        return highestSafeTxnIdForSearch;
    }


    public static void main(String[] args) throws Exception {
        CordraReplicationProcessor processor = new CordraReplicationProcessor(null);
        Service service = new Service();
        service.baseUri = "https://localhost:8443/doip/";
        service.protocol = "DOIP-HTTP";
//        service.username = "admin";
//        service.password = "password";
        service.id = "test/service";
        processor.replicateFrom(service);

        processor.shutdown();
    }
}
