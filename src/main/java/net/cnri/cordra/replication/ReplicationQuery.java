package net.cnri.cordra.replication;

import java.util.List;

public class ReplicationQuery {

    public static String queryFor(Service service, String excludeService, Long highestSafeTxnIdForSearch) {
        String query = queryFor(service, highestSafeTxnIdForSearch);
        query = query + " -userMetadata/cordraReplication/sourceId:\"" + excludeService + "\"";
        return query;
    }

    public static String queryFor(Service service, Long highestSafeTxnIdForSearch) {
        StringBuilder sb = new StringBuilder();
        sb.append(txnIdQueryFor(service.latestTxnId, highestSafeTxnIdForSearch));
        if (service.includeTypes != null && !service.includeTypes.isEmpty()) {
            sb.append(includeTypeQueryFor(service.includeTypes));
        }
        if (service.excludeTypes != null && !service.excludeTypes.isEmpty()) {
            sb.append(excludeTypeQueryFor(service.excludeTypes));
        }
        if (service.customQuery != null) {
            sb.append(customQueryFor(service.customQuery));
        }
        return sb.toString();
    }

    private static String customQueryFor(String customQuery) {
        return " +(" + customQuery + ")";
    }

    public static String txnIdQueryFor(Long lastTxnId, Long highestSafeTxnIdForSearch) {
        //TODO numeric query? but maybe we want this to work with old cordra?
        StringBuilder sb = new StringBuilder();
        long txnId = 0L;
        if (lastTxnId != null) {
            txnId = lastTxnId;
        }
        if (txnId > 0) {
            txnId = txnId + 1;
        }
        sb.append("+metadata/txnId:[\""+ txnId +"\" TO \""+ highestSafeTxnIdForSearch +"\"]");
        return sb.toString();
    }

    public static String typeQueryFor(List<String> types) {
        StringBuilder sb = new StringBuilder();
        sb.append("(");
        boolean isFirst = true;
        for (String type : types) {
            if (isFirst) {
                isFirst = false;
            } else {
                sb.append(" ");
            }
            sb.append("type:\"").append(type).append("\"");
        }
        sb.append(")");
        return sb.toString();
    }
    public static String includeTypeQueryFor(List<String> types) {
        String query = " +" + typeQueryFor(types);
        return query;
    }

    public static String excludeTypeQueryFor(List<String> types) {
        String query = " -" + typeQueryFor(types);
        return query;
    }
}
