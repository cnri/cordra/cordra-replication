package net.cnri.cordra.replication;

import java.util.List;

public class Service {

    public String id; //name or id of the service being pulled from
    public String baseUri;
    public String ipAddress;
    public Integer port;
    public String protocol; //DOIP, DOIP-HTTP, CORDRA-HTTP
    public Long latestTxnId;

    public String error;
    public String lastRunTimestamp;
    public String username; //optional
    public String password; //optional
    public List<String> includeTypes; //optional
    public List<String> excludeTypes; //optional
    public String customQuery; //optional
}
