package net.cnri.cordra.replication;

import java.util.List;

public class CordraReplicationState {

    public Long pollingIntervalSeconds = 60L;
    public List<Service> services;

    public Service getService(String serviceId) {
        for (Service s : services) {
            if (s.id.equals(serviceId)) {
                return s;
            }
        }
        return null;
    }
}
