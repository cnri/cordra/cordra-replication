package net.cnri.cordra.replication;

import net.cnri.cordra.api.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class CordraObjectCopyUtil {

    private static Logger logger = LoggerFactory.getLogger(CordraObjectCopyUtil.class);

    public static void copyRemoteObjectToLocal(CordraObject remoteObject, CordraClient remoteCordra, CordraClient localCordra) throws CordraException {
        if (remoteObject.payloads != null) {
            attachPayloadInputStreams(remoteObject, remoteCordra);
        }
        try {
            CordraObject existingObject = localCordra.get(remoteObject.id);
            boolean isTryUpdateFirst = false;
            if (existingObject != null) {
                isTryUpdateFirst = true;
                if (existingObject.payloads != null) {
                    List<String> payloadsToDelete = calculatePayloadsToDelete(remoteObject, existingObject);
                    for (String payloadName : payloadsToDelete) {
                        remoteObject.deletePayload(payloadName);
                    }
                }
            }
            createOrUpdate(remoteObject, localCordra, isTryUpdateFirst);
        } finally {
            if (remoteObject.payloads != null) {
                for (Payload payload : remoteObject.payloads) {
                    InputStream in = payload.getInputStream();
                    if (in != null) {
                        try {
                            in.close();
                        } catch (Exception e) {
                            logger.warn("Error closing payloads", e);
                        }
                    }
                }
            }
        }
    }

    private static CordraObject createOrUpdate(CordraObject remoteObject, CordraClient localCordra, boolean isTryUpdateFirst) throws CordraException {
        CordraObject result;
        if (isTryUpdateFirst) {
            try {
                result = localCordra.update(remoteObject);
            } catch (NotFoundCordraException e) {
                result = localCordra.create(remoteObject);
            }
        } else {
            try {
                result = localCordra.create(remoteObject);
            } catch (ConflictCordraException e) {
                logger.error("create failed, trying update ", e);
                //What to do if the conflict exception is "Schema name User is not unique."
                //TODO exclude schemas for now but we need more
                result = localCordra.update(remoteObject);
            }
        }
        return result;
    }

    public static List<String> calculatePayloadsToDelete(CordraObject remoteObject, CordraObject existingObject) {
        List<String> payloadsToDelete = new ArrayList<>();
        if (existingObject.payloads != null) {
            for (Payload p : existingObject.payloads) {
                if (remoteObject.getPayload(p.name) == null) {
                    payloadsToDelete.add(p.name);
                }
            }
        }
        return payloadsToDelete;
    }

    public static void attachPayloadInputStreams(CordraObject remoteObject, CordraClient remoteCordra) throws CordraException {
        if (remoteObject.payloads != null) {
            for (Payload p : remoteObject.payloads) {
                @SuppressWarnings("resource")
                InputStream in = remoteCordra.getPayload(remoteObject.id, p.name);
                if (in != null) {
                    p.setInputStream(in);
                }
            }
        }
    }
}
