package net.cnri.cordra.replication;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.cnri.cordra.*;
import net.cnri.cordra.api.CordraException;

@CordraType(name="CordraReplicationState")
public class CordraReplicationStateCordraType implements CordraTypeInterface {

    @CordraMethod
    public static JsonElement stop(@SuppressWarnings("unused") HooksContext context) {
        System.out.println("Stopping replication");
        CordraReplicationProcessor.getInstance().stop();
        JsonObject result = new JsonObject();
        result.addProperty("success", true);
        return result;
    }
    @CordraMethod
    public static JsonElement start(@SuppressWarnings("unused") HooksContext context) {
        System.out.println("Starting replication");
        CordraReplicationProcessor.getInstance().start();
        JsonObject result = new JsonObject();
        result.addProperty("success", true);
        return result;
    }

    @Override
    public void onLoad() throws CordraException {
        System.out.println("CordraReplicationStateCordraType onLoad");
        CordraHooksSupport hooks = CordraHooksSupportProvider.get();
        if (hooks.isThisInstanceLeader()) {
            System.out.println("Starting replication");
            CordraReplicationProcessor.getInstance().start();
        }
    }
    @Override
    public void onBeforeUnload() throws CordraException {
        System.out.println("CordraReplicationStateCordraType onBeforeUnload");
        CordraReplicationProcessor.getInstance().shutdown();
    }
}
